import http.client
import json
from entity import Config

def get_fields(json_data: json, field_name: str):
    fields = set()
    for field in json_data['fields'][field_name]:
        fields.add(field['name'])
    return fields


def add_version(id: str, config: Config, version: str):
    headers = {'Authorization': 'Basic ' + config.jira_user_base64_auth, 'Content-Type': 'application/json'}

    conn = http.client.HTTPSConnection(config.jira_hostname)
    body = json.dumps({
        "update" : {
            "fixVersions" : [{"add" : {"name" : version}}]
        }
    })

    conn.request(method="PUT", body=body, url=config.jira_issue_url+id, headers=headers)
    conn.getresponse()
    conn.close()
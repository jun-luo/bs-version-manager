#!/usr/bin/python3

import argparse, json
import http_helper
from entity import *

parser = argparse.ArgumentParser()
parser.add_argument("-conf", type=str, default='config.json', metavar='Configuration file', required=True)
parser.add_argument("-v", type=str, metavar='Version', required=True)
args = parser.parse_args()

print("loading configuration file ...")
with open(args.conf) as config_file:
    config = Config(json.load(config_file))

issues_file = open(config.issues_file_name, 'r')

print("Adding version to issues ...")

issue_id = issues_file.readline().strip()
while issue_id:
    print(issue_id)
    http_helper.add_version(issue_id, config, args.v)
    issue_id = issues_file.readline().strip().replace(" ", "")
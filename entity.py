import json

class Config(object):
    project_dir: str
    jira_hostname: str
    jira_issue_url: str
    jira_user_base64_auth: str
    jira_user_api_token_auth: str
    issues_file_name: str

    def __init__(self, config: json):
        self.project_dir = config['project-dir']
        self.jira_hostname = config['jira-hostname']
        self.jira_issue_url = config['jira-issue-url']
        self.jira_user_base64_auth = config['jira-user-base64-auth']
        self.issues_file_name = config['issues-file-name']
